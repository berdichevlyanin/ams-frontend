'use strict';

let webpackConfig = () => {
    const environment = process.env.NODE_ENV || 'development';

    const configs = {
        global: require(`${__dirname}/webpack-configs/global.config`),
        environment: require(`${__dirname}/webpack-configs/environments/${environment}.config`)
    };

    if (!configs.global || !configs.environment) {
        throw new Error('Can\'t find enviroment\'s. See configs object');
    }

    return Object.assign(configs.global(__dirname), configs.environment(__dirname, environment));
};

module.exports = webpackConfig();