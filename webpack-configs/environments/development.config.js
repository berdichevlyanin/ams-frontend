'use strict';

const path = require('path');
const jade = require('jade');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');
const TextPlugin = require('extract-text-webpack-plugin');

module.exports = (_path, _environment) => {
    const srcFolderPath = path.resolve(_path, 'src', 'client');
    const rootAppPath = path.join(srcFolderPath, 'app');
    const hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';

	return {
        entry: {
            app: [ path.join(rootAppPath, 'core', 'bootstrap.js'), hotMiddlewareScript ]
        },
        debug: true,
        devtool: 'cheap-module-inline-source-map',
        plugins: [
            new HtmlPlugin({
                title: 'AMS prototype app',
                filename: 'index.html',
                templateContent: function (templateParams, compilation) {
                    const indexTemplate = path.join(srcFolderPath, 'index.jade');
                    compilation.fileDependencies.push(indexTemplate);

                    return jade.compileFile(indexTemplate, {pretty: true})();
                }
            }),
            new TextPlugin('css/[name].bundle.css'),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                moment: 'moment'
            }),
            new webpack.optimize.OccurenceOrderPlugin(true),
            new webpack.HotModuleReplacementPlugin({quiet: true}),
            new webpack.NoErrorsPlugin(),
            new webpack.DefinePlugin({
                NODE_ENV: JSON.stringify(_environment)
            })
        ]
    }
};
