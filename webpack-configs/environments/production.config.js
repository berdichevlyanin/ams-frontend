'user strict';

const path = require('path');
const jade = require('jade');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');
const TextPlugin = require('extract-text-webpack-plugin');
const ngAnnotatePlugin = require('ng-annotate-webpack-plugin');

module.exports = (_path, _environment) => {
    const dependencies = Object.keys(require(_path + '/package').dependencies);
    const srcFolderPath = path.resolve(_path, 'src', 'client');
    const rootAppPath = path.join(srcFolderPath, 'app');

    return {
        entry: {
                app: path.join(rootAppPath, 'core', 'bootstrap.js'),
                vendors: dependencies
            },
        debug: false,
        plugins: [
            new webpack.optimize.DedupePlugin(),
            new ngAnnotatePlugin({add: true}),
            new webpack.optimize.CommonsChunkPlugin('vendors', 'js/[name].bundle.js'),
            new webpack.optimize.OccurenceOrderPlugin(true),
            new HtmlPlugin({
                title: 'AMS prototype app',
                chunks: ['app', 'vendors'],
                filename: 'index.html',
                templateContent: function (templateParams, compilation) {
                    const indexTemplate = path.join(srcFolderPath, 'index.jade');
                    compilation.fileDependencies.push(indexTemplate);

                    return jade.compileFile(indexTemplate)();
                }
            }),
            new TextPlugin('css/[name].bundle.css', {allChunks: true}),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                moment: 'moment'
            }),
            new webpack.DefinePlugin({
                NODE_ENV: JSON.stringify(_environment)
            }),
            new webpack.optimize.UglifyJsPlugin({
                sourceMap: false,
                mangle: false,
                unsafe: true,
                compress: {
                    drop_console: true,
                    warnings: false
                },
                output: {
                    comments: false
                }
            })
        ]
    }
};
