'use strict';

const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const csswring = require('csswring');
const TextPlugin = require('extract-text-webpack-plugin');

module.exports = (_path) =>{
    const path = require('path');
    const nodeModulesPath = path.resolve(_path, 'node_modules');
    const srcFolderPath = path.resolve(_path, 'src', 'client');
    const rootAppPath = path.join(srcFolderPath, 'app');

    return {
        cache: true,
        context: rootAppPath,
        output: {
            path: path.join(_path, 'public'),
            publicPath: '/',
            //filename: 'js/[name]-[hash].bundle.js', - if needed hash
            filename: 'js/[name].bundle.js',
            library: '[name]'
        },

        resolve: {
            root: [
                srcFolderPath,
                path.resolve(_path, 'node_modules')
            ],
            modulesDirectories: ['node_modules'],
            extensions: ['', '.js', '.jade', '.html', '.css', '.scss', '.sass'],
            alias: {
                'assets': path.join(srcFolderPath, 'assets'),
                'formly-lumx': path.resolve(nodeModulesPath, 'angular-formly-templates-lumx', 'dist'),
                'mdi': path.resolve(nodeModulesPath, 'mdi')
            }
        },

        postcss: [
            autoprefixer({
                browsers: ['last 3 versions']
            }),
            csswring({
                map: true,
                preserveHacks: true,
                removeAllComments: true
            })
        ],
        jshint: {
            curly: true,
            failOnHint: true,
            reporter: function (errors) {
                console.log(errors);
            }
        },

        module: {
            preLoaders: [
                {
                    test: /\.js$/,
                    include: 'src',
                    loader: 'jshint-loader'
                }
            ],
            loaders: [
                {
                    test: /\.jade$/,
                    exclude: /index\/.jade$/,
                    loader: 'jade'
                },
                {
                    test: /\.css$/,
                    loader: TextPlugin.extract('style', 'css-loader!postcss-loader')
                },
                {
                    test: /\.scss$/,
                    loader: TextPlugin.extract('style', 'css-loader!postcss-loader?sourceMap!sass-loader')
                },
                {
                    test: /\.(ttf|eot|woff|woff2|png|ico|jpg|jpeg|gif|svg)(\?]?.*)?$/,
                    loaders: ['file?context=' + rootAppPath + '&name=static/[ext]/[name].[ext]']
                },
                {
                    test: /\.json?$/,
                    loader: 'json'
                }
            ],
            noParse: [
                /angular\/angular.js/,
                /jquery\/dist\/jquery.js/,
                /moment\/moment.js/
            ]
        }
    };
};
