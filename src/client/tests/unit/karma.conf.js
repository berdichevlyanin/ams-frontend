module.exports = function (config) {
    config.set({
        basePath: '../../../',
        frameworks: [ 'jasmine' ],
        plugins: [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-phantomjs-launcher', 'karma-jasmine',
            'karma-junit-reporter',
            'karma-jasmine',
            'karma-webpack'
        ],
        junitReporter: {
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
        },
        webpack: {
            module: {
                loaders: [
                    { test: /\.scss$/, exclude: /node_modules/, loader: 'style-loader!css-loader!sass-loader' },
                    { test: /\.jade$/, exclude: /node_modules/, loader: 'jade-loader' }
                ]
            },
            watch: true
        },
        webpackMiddleware: {
            noInfo: true
        },
        files: [
            './node_modules/phantomjs-polyfill/bind-polyfill.js',
            './node_modules/angular/angular.js',
            './node_modules/angular-mocks/angular-mocks.js',
            './src/tests/spec.js'
        ],
        exclude: [],
        preprocessors: {
            './src/client/tests/spec.js': [ 'webpack' ]
        },
        reporters: [ 'progress' ],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: [ 'PhantomJS' ],
        // browsers: [ 'Chrome' ],
        singleRun: false
    });
};
