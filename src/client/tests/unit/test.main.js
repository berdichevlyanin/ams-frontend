var allTestFiles = [];
var TEST_REGEXP = /(_spec|_test)\.js$/i;

window.__karma__.files.forEach(function(file){
    if (TEST_REGEXP.test(file)) {
        allTestFiles.push(file);
    }
});
