module.exports = function(){
	//Dependencies styles
	require('mdi/scss/materialdesignicons.scss');
	require('node-lumx/dist/scss/_lumx.scss');
	//Project common styles
	require('assets/styles/all.scss');
};
