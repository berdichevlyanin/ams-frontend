'use strict';

require('./dependencies.import')();
require('./styles.import')();

var app = require('../app');

angular.element(document).ready(function(){
  angular.bootstrap(document, [ app ])
});
