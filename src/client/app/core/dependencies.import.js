module.exports = function(){
	global.$ = global.jQuery = require('jquery');
	require('velocity-animate');
	require('angular');
	require('angular-aria');
	require('angular-messages');
	require('angular-formly');
	require('angular-ui-router');
	global.moment = require('moment');
	require('node-lumx');
	//require('formly-lumx/formlyLumx');
};

