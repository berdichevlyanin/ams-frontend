'use strict';

module.exports = angular.module('AMS', [
    'ui.router',
    'ngMessages',
    'ngAria',
    'formly',
    'lumx',
    //'formlyLumx',
    require('./common'),
    require('./modules')
]).name;
