'use strict';

module.exports = angular.module('AMS.modules', [
	require('./useful-links')
])
.config(require('./modules.route'))
.controller(require('./modules.controller'))
.name;