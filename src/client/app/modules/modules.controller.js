'use strict';

module.exports = function(LxDialogService, LxNotificationService){
	"ngInject";

	var vm = this;

	vm.LxDialogService = LxDialogService;
	vm.LxNotificationService = LxNotificationService;
	vm.showYodaPhoto = showYodaPhoto;

	function showYodaPhoto(dialogId){
		vm.LxDialogService.open(dialogId);
	}

	function hideYodaPhoto() {
		vm.LxNotificationService.info('Dialog closed!');
	}
};
