'use strict';
require('./assets/styles/useful-links.scss');

module.exports = angular.module('AMS.usefulLinks', [])
	.config(require('./useful-links.route'))
	.controller(require('./useful-links.controller'))
	.name;