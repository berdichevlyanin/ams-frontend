'use strict';

module.exports = function($stateProvider, appConfig){
	"ngInject";
	var currentState = appConfig.states.app.modules.usefulLinks;

	$stateProvider
		.state(currentState.state, {
			url: currentState.url,
			controller: require('./useful-links.controller'),
			controllerAs: 'vm',
			template: require('./useful-links.tpl.jade')
		})
};