'use strict';

module.exports = function(){
	"ngInject";

	var vm = this;

	vm.usefulLinks = {
		formlyLumx: {
			heading: 'Formly Lumx',
			url: 'https://www.npmjs.com/package/angular-formly-templates-lumx',
			img: 'npm-logo'
		},
		html2jade: {
			heading: 'Html2Jade',
			url: 'http://html2jade.org/',
			img: 'jade-logo'
		},
		lumx: {
			heading: 'Lumx',
			url: 'http://ui.lumapps.com/',
			img: 'lumx-logo'
		}
	}
};
