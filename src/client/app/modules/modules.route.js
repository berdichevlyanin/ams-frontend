'use strict';

module.exports = function($stateProvider, appConfig){
	"ngInject";
	var currentState = appConfig.states.app;

	$stateProvider
		.state(currentState.abstractState, {
			url: currentState.url,
			//abstract: true,
			controller: require('./modules.controller'),
			controllerAs: 'vm',
			template: require('./index.jade')
		})
};