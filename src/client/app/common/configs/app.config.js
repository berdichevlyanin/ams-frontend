var appConfig = {};

appConfig.states = {
	app: {
		url: '/',
		abstractState: 'app',
		modules: {
			usefulLinks: {
				url: 'useful-links',
				state: 'app.usefulLinks'
			}
		}
	}
};

exports.appConfig = appConfig;