'use strict';

module.exports = angular.module('AMS.common', [])
	.constant('appConfig', require('./configs/app.config').appConfig)
	.config(require('./configs/router.config'))
	.name;