'use strict';

const config = require('./settings');
const webpackDevServer = require('../webpackDevServer');

module.exports = (app, express, webroot) => {
    if (config.devMode) {
        console.log(config.port);
        webpackDevServer(app);
    } else {
        app.use(express.static(`${ webroot }/public`));
    }
};
