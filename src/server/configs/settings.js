'use strict';

let config = {};

config.devMode = !Object.is(process.env.NODE_ENV, 'production');
config.port = config.devMode ? 3000 : process.env.PORT;

module.exports = config;
