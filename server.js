'use strict';

const express = require('express');
const config = require('./src/server/configs/settings');
const environment = require('./src/server/configs/environment');

const app = express();

environment(app, express, __dirname);

app.listen(config.port, 'localhost', function onStart(err) {
	if (err) {
		console.log(err);
	}
	console.info('==> 🌎 Listening on port %s. Open up http://localhost:%s/ in your browser.', config.port, config.port);
});
