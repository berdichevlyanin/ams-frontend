# ClientSide Application
***

Clone git repo: `git clone "git@gitlab.com/berdichevlyanin/ams-frontend.git"`

## Quick Start
### install NodeJs https://nodejs.org/
You need to download and install NodeJS latest version, then check `npm` in console. If command not working, configure system path and refresh OS session. All configuration files and server uses es6 syntax.

### Starting application with webpack-hot-module-replacement mode

$ `npm start`

As prestart command for this operation used $ `npm start`. Default port for application server used 3000. In case of success, you would see next lines in console

$ `==> 🔆 Starting Webpack development serverv`'

$`==> 🌎 Listening on port 3000. Open up http://localhost:3000/ in your browser.`

### Building application with webpack for production

$ `npm run build`

In this case, webpack would bundle all application and used dependencies resources as a single `[name].bundle.js`. In root folder would be created folder `/public`. In this folder you can find `[name].bundle.js` files. Server use bundles as static resources

### Project structure

```
ams-frontend
  |-- src/
     |-- client/
         |-- app/
             |--common/
                |-- configs/
                    |-- first_common.config.js
                    ----  ******************  ----
                    |-- last_common.config.js
                |-- index.js
     |-- core/
     	 |-- bootstrap.js
     	 |-- dependencies.import.js
     	 |-- styles.import.js
     |-- modules/
     	 |-- index.jade
     	 |-- index.js
     	 |-- modules.controller.js
     	 |-- modules.route.js
     |-- app.js
  |-- assets/
      |-- styles/
          |-- all.scss
          |-- app.scss
      |-- media/
          |-- fonts/
          |-- images/
  |-- tests/
      |-- e2e/
          |-- protractor.conf.js
          |-- scenarios.js
      |-- unit/
      	  |-- karma.conf.js
      	  |-- spec.js
      	  |-- test.main.js
  |-- index.jade
  |-- meta.jade
```
